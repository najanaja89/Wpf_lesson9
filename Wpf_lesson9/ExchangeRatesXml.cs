﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Wpf_lesson9
{

    [Serializable()]
    [XmlRoot(ElementName = "item")]
    public class ExchangeRatesXml
    {
        [XmlElement(ElementName = "title")]
        public string Title { get; set; }
        [XmlElement(ElementName = "pubDate")]
        public string PubDate { get; set; }
        [XmlElement(ElementName = "description")]
        public string Description { get; set; }
        [XmlElement(ElementName = "quant")]
        public string Quant { get; set; }
        [XmlElement(ElementName = "index")]
        public string Index { get; set; }
        [XmlElement(ElementName = "change")]
        public string Change { get; set; }
        [XmlElement(ElementName = "link")]
        public string Link { get; set; }
    }

}
