﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Xml.Serialization;
using System.Net;
using Wpf_lesson9;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using Newtonsoft.Json;
using System.Data;

namespace Wpf_lesson9
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //List<ExchangeRates> ratesList;
            //ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => true;
            //var webClient = new WebClient();
            //string jsonString = webClient.DownloadString("https://data.egov.kz/api/v2/valutalar_bagamdary4/v571?pretty");
            //ratesList = JsonConvert.DeserializeObject<List<ExchangeRates>>(jsonString);
            //webClient.Dispose();
            //exchangeGrid.ItemsSource = ratesList;

            

            string  ratesXml = @"https://nationalbank.kz/rss/rates_all.xml?switch=russian";
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(ratesXml);
            DataView dataView = new DataView(dataSet.Tables[2]);
            dataView.Table.Columns[0].ColumnName = "Название";
            dataView.Table.Columns[1].ColumnName = "Дата публикации";
            dataView.Table.Columns[2].ColumnName = "Курс";
            dataView.Table.Columns[3].ColumnName = "Соотношение";
            dataView.Table.Columns[4].ColumnName = "Позиция";
            dataView.Table.Columns[5].ColumnName = "Статус";
            dataView.Table.Columns.Remove("link");
            
            exchangeGrid.ItemsSource = dataView;
  

        }

        private void refreshButtonClick(object sender, RoutedEventArgs e)
        {
            string ratesXml = @"https://nationalbank.kz/rss/rates_all.xml?switch=russian";
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(ratesXml);
            DataView dataView = new DataView(dataSet.Tables[2]);
            dataView.Table.Columns[0].ColumnName = "Название";
            dataView.Table.Columns[1].ColumnName = "Дата публикации";
            dataView.Table.Columns[2].ColumnName = "Курс";
            dataView.Table.Columns[3].ColumnName = "Соотношение";
            dataView.Table.Columns[4].ColumnName = "Позиция";
            dataView.Table.Columns[5].ColumnName = "Статус";
            dataView.Table.Columns.Remove("link");
            exchangeGrid.ItemsSource = null;
            exchangeGrid.ItemsSource = dataView;
        }
    }
}
